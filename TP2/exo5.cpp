#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)

	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
    int o_size = origin.size();
    if( o_size <= 1) return;
    for(unsigned int i = 0; i< o_size; i++){
        if(i < first.size()) first[i] = origin[i];
        else second[i-first.size()] = origin[i];
    }

	// recursiv splitAndMerge of lowerArray and greaterArray
    splitAndMerge(first);
    splitAndMerge(second);

	// merge
    Array& result = w->newArray(first.size() + second.size());
    merge(first, second, result);
    origin = result;
}

void merge(Array& first, Array& second, Array& result)
{
    unsigned int i = 0, cpt_f = 0, cpt_s = 0;
    unsigned int o_size = first.size() + second.size();
    while(i < o_size && cpt_f < first.size() && cpt_s < second.size() ){
        if(first[cpt_f] < second[cpt_s]){
            result[i] = first[cpt_f];
            cpt_f++;
        }
        else{
            result[i] = second[cpt_s];
            cpt_s++;
        }
        i++;

    }

    if(cpt_f < first.size()){
        for(; i < o_size; i++){
            result[i] = first[cpt_f];
            cpt_f++;
        }
    }
    else {
        for(; i < o_size; i++){
            result[i] = second[cpt_s];
            cpt_s++;
        }
    }

}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
