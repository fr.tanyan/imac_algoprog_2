#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot

    // check n

    // check length of z
    // if Mandelbrot, return 1 or n (check the difference)
    // otherwise, process the square of z and recall
    // isMandebrot

    /*
     *  retourne vrai si le point appartient à l’ensemble de
     * Mandelbrot pour la fonction f (z) → z² + point. Un point appartient à cet ensemble si la
     * suite zn est bornée, autrement-dit s’il existe un i < n tel que |zi| > 2. Attention, z est un
     * complexe, autrement-dit il représente un réel et un imaginaire x + iy et donc z² = (x + iy)².
     * Posez le calcul et déterminer le nouveau réel et le nouvel imaginaire.
     */

        if (n <= 0 || z.length()>2){
            return 0;
        } else {
            z = {(z.x * z.x) - (z.y * z.y) + point.x , 2 * z.x * z.y + point.y};
            return ( 1 + isMandelbrot(z,n -1, point));
        }
        return 0;


}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



