#include <cstddef>
#include <iostream>

using namespace std;

struct Noeud {
  int donnee;
  Noeud *suivant;
};

struct Liste {
  Noeud *premier;
  Liste(){
      this->premier = nullptr;
  }
};

struct DynaTableau {
  int *donnees;
  int capacite;
  int remplissage;

  DynaTableau(){};
  DynaTableau(int cap): capacite(cap), remplissage(0) {
    donnees = new int[cap];
  }
};

void initialise(Liste * liste) {
  liste = new Liste();
}

bool est_vide(const Liste *liste) {
  return liste->premier == nullptr;
}

void ajoute(Liste *liste, int valeur) {
    if(est_vide(liste)){
        liste->premier = new Noeud ();
        liste->premier->donnee = valeur;
    }
    else{
        Noeud * tmp = liste->premier;
        while(tmp->suivant != nullptr){
            tmp = tmp->suivant;
        }
        tmp->suivant = new Noeud ();
        tmp->suivant->donnee = valeur;
    }
}

void affiche(const Liste *liste) {
    Noeud * tmp = liste->premier;
    while(tmp != nullptr){
        cout << tmp->donnee << "\n";
        tmp = tmp->suivant;
    }
    cout << "\n";
}

int recupere(const Liste *liste, int n) {
    int cpt = 0;
    Noeud * tmp = liste->premier;
    while(tmp != nullptr && cpt < n){
        tmp = tmp->suivant;
        cpt++;
    }
    return tmp->donnee;
}

int cherche(const Liste *liste, int valeur) {
    int cpt = 0;
    Noeud * tmp = liste->premier;
    while(tmp != nullptr){
        if(tmp->donnee == valeur) return cpt;
        tmp = tmp->suivant;
        cpt++;
    }
    return -1;
}

void stocke(Liste *liste, int n, int valeur) {
    Noeud * tmp = liste->premier;
    int cpt = 0;
    while(tmp != nullptr && cpt < n){
        tmp = tmp->suivant;
        cpt++;
    }
    tmp->donnee = valeur;
}

void ajoute(DynaTableau *tableau, int valeur) {

    if(tableau->remplissage == tableau->capacite){
        tableau->capacite *=2;
        tableau->donnees = (int *)realloc(tableau->donnees, tableau->capacite * sizeof(int*));
    }
    tableau->donnees[tableau->remplissage] = valeur;
    tableau->remplissage +=1;

}

void initialise(DynaTableau *tableau, int capacite) {
    *tableau = DynaTableau(capacite);
}

bool est_vide(const DynaTableau *tableau) {

    return tableau->remplissage==0; }

void affiche(const DynaTableau *tableau) {
    for(int i=0; i<tableau->remplissage; i++){
        cout << tableau->donnees[i] << "\n";
    }
}

int recupere(const DynaTableau *tableau, int n) {
    return tableau->donnees[n];
}

int cherche(const DynaTableau *tableau, int valeur) {
    for(int i=0; i<tableau->remplissage; i++){
        if(tableau->donnees[i]==valeur) return i;
    }
    return -1; }

void stocke(DynaTableau *tableau, int n, int valeur) {
    tableau->donnees[n] = valeur;
}

// void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste *liste, int valeur) {//Ajout à la fin
    ajoute(liste, valeur);
}

// int retire_file(Liste* liste)
int retire_file(Liste *liste) {
    //enlève au début
    int tmp = liste->premier->donnee;
    liste->premier = liste->premier->suivant;
    return tmp;
}

// void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste *liste, int valeur) { //ajout au début
    if(est_vide(liste)){
        liste->premier = new Noeud ();
        liste->premier->donnee = valeur;
    }

    liste -> premier = new Noeud{valeur, liste->premier};
}

// int retire_pile(DynaTableau* liste)
int retire_pile(Liste *liste) {

    return retire_file(liste); }

int main() {
  Liste liste;
  initialise(&liste);

  DynaTableau tableau;
  initialise(&tableau, 5);

  if (!est_vide(&liste)) {
    std::cout << "Oups y a une anguille dans ma liste" << std::endl;
  }

  if (!est_vide(&tableau)) {
    std::cout << "Oups y a une anguille dans mon tableau v" << std::endl;
  }

  for (int i = 1; i <= 7; i++) {
    ajoute(&liste, i * 7);
    ajoute(&tableau, i * 5);
  }

  if (est_vide(&liste)) {
    std::cout << "Oups y a une anguille dans ma liste" << std::endl;
  }

  if (est_vide(&tableau)) {
    std::cout << "Oups y a une anguille dans mon tableau c" << std::endl;
  }

  std::cout << "Elements initiaux:" << std::endl;
  affiche(&liste);
  affiche(&tableau);
  std::cout << std::endl;

  std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
  std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

  std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21)
            << std::endl;
  std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15)
            << std::endl;

  stocke(&liste, 4, 7);
  stocke(&tableau, 4, 7);

  std::cout << "Elements après stockage de 7:" << std::endl;
  affiche(&liste);
  affiche(&tableau);
  std::cout << std::endl;

  Liste pile; // DynaTableau pile;
  Liste file; // DynaTableau file;

  initialise(&pile);
  initialise(&file);

  for (int i = 1; i <= 7; i++) {
    pousse_file(&file, i);
    pousse_pile(&pile, i);
  }

  cout << "affiche file\n";
  affiche(&file);
  cout << "affiche pile\n";
  affiche(&pile);

  int compteur = 10;
  while (!est_vide(&file) && compteur > 0) {
    std::cout << retire_file(&file) << std::endl;
    compteur--;
  }

  if (compteur == 0) {
    std::cout << "Ah y a un soucis là..." << std::endl;
  }

  compteur = 10;
  while (!est_vide(&pile) && compteur > 0) {
    std::cout << retire_pile(&pile) << std::endl;
    compteur--;
  }

  if (compteur == 0) {
    std::cout << "Ah y a un soucis là..." << std::endl;
  }

  return 0;
}
