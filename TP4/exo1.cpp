#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChildIndex(int nodeIndex) // retourne l’index du fils gauche du nœud à l’indice nodeIndex
{
    //i × 2 + 1
    return nodeIndex * 2 + 1;
}

int Heap::rightChildIndex(int nodeIndex) // retourne l’index du fils droit du nœud à l’indice nodeIndex
{// i × 2 + 2
    return nodeIndex * 2 + 2;
}

void Heap::insertHeapNode(int heapSize, int value)
// insère un nouveau noeud dans le tas heap
//tout en gardant la propriété de tas
{
    int i = heapSize;
    this->get(i) = value;
    while(i>0 && get(i) > get((i-1)/2)){
        swap(i, (i-1)/2);
        i = (i-1)/2;
    }

}

void Heap::heapify(int heapSize, int nodeIndex)
// Si le noeud à l’indice nodeIndex n’est pas supérieur
//à ses enfants, reconstruit le tas à partir de cette index
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i_max = nodeIndex;
    int i_left = leftChildIndex(i_max);
    int i_right = rightChildIndex(i_max);

    if (i_left < heapSize && get(i_left) > get(i_max)) i_max = i_left;
    if (i_right < heapSize && get(i_right) > get(i_max)) i_max = i_right;

    if (i_max != nodeIndex) {
        swap(nodeIndex, i_max);
        heapify(heapSize, i_max);
    }
    /*
this ⇐ Tas de taille n
nodeIndex ⇐ indice de la racine
i_max ⇐ indice de la valeur la plus grande entre le nœud i et ses enfants
Si i_max != nodeIndex Alors
swap(heap[nodeIndex], heap[i_max])
this.heapify(heapSize, i_max)
fin S
     */
}

void Heap::buildHeap(Array& numbers)
// Construit un tas à partir des valeurs de numbers
//(vous pouvez utileftChildliser soit insertHeapNode soit heapify)
{
    for(unsigned int i =0; i<numbers.size(); i++){
        get(size()) = numbers[i];
    }
    for(unsigned int i = (size() / 2) - 1; i >= 0; i--){
        heapify(size(), i);
    }


}

void Heap::heapSort() //Construit un tableau trié à partir d’un tas heap
{
    /*
     *
this ⇐ tas créé à partir d’un tableau de n nombres aléatoires
Pour i allant de n − 1 à 0 faire
swap(heap[0], heap[i])
heapif y(heap, i, 0)
fin Pou
     */
    for (int i = size() - 1; i >= 0; i--) {
        swap(0, i);
        heapify(i, 0);
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
    w->show();

    return a.exec();
}
